#!/usr/bin/lua
local _FILENAME = "mail_bwg.lua"
local _AUTHOR = "marco (@macarony.de - Matthias Diener)"
local _VERSION = "20151006"

getVersion = function() return _VERSION, _AUTHOR, _FILENAME; end

local DATA_FILE = "/etc/mail_bwg.dat.lua"

local t_data = {}
local data = loadfile(DATA_FILE, "bt", setmetatable(t_data, {__index = _G}))
if not(pcall(data)) then
  io.write(string.format("ERROR: Cannot parse data file: \"%s\"\n", DATA_FILE))
  os.exit(1)
end
setmetatable(t_data, nil)

if not(t_data.redis_dsn) or not(t_data.redis_prefix) then
  io.write("ERROR: Redis not configured\n")
  os.exit(1)
end
if arg[1] == "l" then
  local r = require("redis").connect(t_data.redis_dsn)
  local t_res = r:q({{"KEYS", string.format("%s*", t_data.redis_prefix)}})[1]
  table.sort(t_res)
  for i = 1, #t_res do
    print(string.sub(t_res[i], #t_data.redis_prefix + 1))
  end
  r:close()
  os.exit(0)
elseif arg[1] == "d" then
  local r = require("redis").connect(t_data.redis_dsn)
  r:q({{"DEL", string.format("%s%s", t_data.redis_prefix, arg[2])}})
  r:close()
  os.exit(0)
elseif arg[1] == "a" then
  local r = require("redis").connect(t_data.redis_dsn)
  r:q({{"SETEX", string.format("%s%s", t_data.redis_prefix, arg[2]), t_data.grey_white, 1}})
  r:close()
  os.exit(0)
end
t_data.grey_min = t_data.grey_min or 300 -- 5m
t_data.grey_max = t_data.grey_max or 86400 -- 1d
t_data.grey_white = t_data.grey_white or 3024000 -- 5w

t_data.win_status = string.lower(t_data.win_status or "black")

t_data.sender = t_data.sender or {}
t_data.recipient = t_data.recipient or {}

local n_timestamp = os.time()
local t_sender, t_recipient, t_server
local _, n_status = string.gsub(arg[1] or "", "^([^%[]+)%[([^%]]+)%]([^@/]*)@?([^/]*)/([^@]+)@(.*)$",
  function(is_server_name, is_server_ip, is_sender_local_part, is_sender_domain, is_recipient_local_part, is_recipient_domain)
    t_server = {name = is_server_name, ip = is_server_ip}
    t_sender = {local_part = is_sender_local_part, domain = is_sender_domain}
    t_recipient = {local_part = is_recipient_local_part, domain = is_recipient_domain}
  end
)
if n_status == 0 then
  io.write("ERROR: argument not valid. \"server_name[server_ip]sender/recipient\"\n")
  os.exit(1)
end

local s_status_sender = t_data.sender[t_sender.domain] and (t_data.sender[t_sender.domain][t_sender.local_part] or t_data.sender[t_sender.domain][1] or "grey") or t_data.sender[1] and (t_data.sender[1][t_sender.local_part] or t_data.sender[1][1]) or "grey"
local s_status = t_data.recipient[t_recipient.domain] and (t_data.recipient[t_recipient.domain][t_recipient.local_part] or t_data.recipient[t_recipient.domain][1] or "grey") or t_data.recipient[1] and (t_data.recipient[1][t_recipient.local_part] or t_data.recipient[1][1]) or "grey"
if (s_status_sender == t_data.win_status) or (s_status == "grey") then
  s_status = s_status_sender
end
if not(s_status == "grey") then
  io.write(s_status)
  os.exit(0)
end

local r = require("redis").connect(t_data.redis_dsn)
local t_result = r:q({{"MGET", string.format("%s%s[%s]", t_data.redis_prefix, t_server.name, t_server.ip), string.format("%s%s", t_data.redis_prefix, arg[1])}})[1]
if (t_result[1]) or (t_result[2] and ((t_result[2] + t_data.grey_min) < n_timestamp)) then
  r:q({{"SETEX", string.format("%s%s[%s]", t_data.redis_prefix, t_server.name, t_server.ip), t_data.grey_white, tonumber(t_result[1] or 0) + 1}})
  s_status = "white"
elseif not(t_result[2]) then
  r:q({{"SETEX", string.format("%s%s", t_data.redis_prefix, arg[1]), t_data.grey_max, n_timestamp}})
end
r:close()
io.write(s_status)
os.exit(0)


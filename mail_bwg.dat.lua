package.path = string.format("/usr/lib/lua/?.luac;%s", package.path)

redis_dsn = "redis://:6379"
redis_prefix = "grey://"

grey_min = 300 -- 5m
grey_max = 86400 -- 1d
grey_white = 3024000 -- 5w

win_status = "black" -- "black|white"

sender = {
  ["spamdomain.com"] = {
    "black",
  },
  {
    ["spamsender"] = "black",
  },
}

recipient = {
  ["example.com"] = {
    ["spam"] = "black",
    ["info"] = "white",
  },
}

